package com.edugaon.spinner

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val genderArray = arrayOf("Select", "Male", "Female", "Transparent")
        val genderAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item , genderArray)
        val spinner = findViewById<Spinner>(R.id.gender_spinner)
        spinner.adapter = genderAdapter
    }
}